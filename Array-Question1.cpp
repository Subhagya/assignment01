// c program to Reverse a sentence entered by the user using string

    #include<stdio.h>
    #include<string.h>

    int main(){
	
	char s[100];
	
	printf("Enter a string to reverse\n");
	
	gets(s);
	
	strrev(s);
	
	printf("Reverse the string: %s\n", s);
	
	
    }
